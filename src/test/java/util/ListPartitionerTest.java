package util;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.tcb.common.util.ListPartitioner;

public class ListPartitionerTest {
	
	private List<Double> list;

	@Before
	public void setUp() throws Exception {
		this.list = Arrays.asList(0.1, 0.2, 0.3, 0.4, 0.5, 0.6);
	}
	
	@Test
	public void testPartition1() {
		List<List<Double>> partitionedRef = Arrays.asList(
				Arrays.asList(0.1),
				Arrays.asList(0.2),
				Arrays.asList(0.3),
				Arrays.asList(0.4),
				Arrays.asList(0.5),
				Arrays.asList(0.6));
		List<List<Double>> partitioned = ListPartitioner.equal_partition(this.list, 1);
		
		assertEquals(partitionedRef,partitioned);
	}

	@Test
	public void testPartition2() {
		List<List<Double>> partitionedRef = Arrays.asList(
				Arrays.asList(0.1,0.2),
				Arrays.asList(0.3,0.4),
				Arrays.asList(0.5,0.6));
		List<List<Double>> partitioned = ListPartitioner.equal_partition(this.list, 2);
		
		assertEquals(partitionedRef,partitioned);
	}
	
	@Test
	public void testPartition3() {
		List<List<Double>> partitionedRef = Arrays.asList(
				Arrays.asList(0.1,0.2,0.3),
				Arrays.asList(0.4,0.5,0.6));
		
		List<List<Double>> partitioned = ListPartitioner.equal_partition(this.list, 3);
		
		assertEquals(partitionedRef,partitioned);
	}
	
	@Test
	public void testPartition4(){
		List<List<Double>> partitionedRef = Arrays.asList(
				Arrays.asList(0.1,0.2,0.3,0.4));
		
		List<List<Double>> partitioned = ListPartitioner.equal_partition(this.list, 4);
		
		assertEquals(partitionedRef,partitioned);
	}
	
	@Test
	public void testPartition5(){
		List<List<Double>> partitionedRef = Arrays.asList(
				Arrays.asList(0.1,0.2,0.3,0.4,0.5));
		
		List<List<Double>> partitioned = ListPartitioner.equal_partition(this.list, 5);
		
		assertEquals(partitionedRef,partitioned);
	}
	
	@Test
	public void testPartition6(){
		List<List<Double>> partitionedRef = Arrays.asList(
				Arrays.asList(0.1,0.2,0.3,0.4,0.5,0.6));
		
		List<List<Double>> partitioned = ListPartitioner.equal_partition(this.list, 6);
		
		assertEquals(partitionedRef,partitioned);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testPartition7(){
		List<List<Double>> partitioned = ListPartitioner.equal_partition(this.list, 7);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testPartition0(){
		List<List<Double>> partitioned = ListPartitioner.equal_partition(this.list, 0);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testPartitionEmptyList(){
		List<Double> emptyList = new ArrayList<Double>();

		List<List<Double>> partitioned = ListPartitioner.equal_partition(emptyList, 2);
	}
	
	@Test
	public void testPartition1_unequal(){
		List<List<Double>> partitionedRef = Arrays.asList(
				Arrays.asList(0.1),
				Arrays.asList(0.2),
				Arrays.asList(0.3),
				Arrays.asList(0.4),
				Arrays.asList(0.5),
				Arrays.asList(0.6));
		
		List<List<Double>> partitioned = ListPartitioner.unequal_partition(this.list, 1);
		
		assertEquals(partitionedRef,partitioned);
	}
	
	
	
	@Test
	public void testPartition2_unequal(){
		List<List<Double>> partitionedRef = Arrays.asList(
				Arrays.asList(0.1,0.2),
				Arrays.asList(0.3,0.4),
				Arrays.asList(0.5,0.6));
		
		List<List<Double>> partitioned = ListPartitioner.unequal_partition(this.list, 2);
		
		assertEquals(partitionedRef,partitioned);
	}
	
	
	@Test
	public void testPartition3_unequal(){
		List<List<Double>> partitionedRef = Arrays.asList(
				Arrays.asList(0.1,0.2,0.3),
				Arrays.asList(0.4,0.5,0.6));
		
		List<List<Double>> partitioned = ListPartitioner.unequal_partition(this.list, 3);
		
		assertEquals(partitionedRef,partitioned);
	}
	
	
	@Test
	public void testPartition4_unequal(){
		List<List<Double>> partitionedRef = Arrays.asList(
				Arrays.asList(0.1,0.2,0.3,0.4),
				Arrays.asList(0.5,0.6));
		
		List<List<Double>> partitioned = ListPartitioner.unequal_partition(this.list, 4);
		
		assertEquals(partitionedRef,partitioned);
	}
	
	@Test
	public void testPartition5_unequal(){
		List<List<Double>> partitionedRef = Arrays.asList(
				Arrays.asList(0.1,0.2,0.3,0.4,0.5),
				Arrays.asList(0.6));
		
		List<List<Double>> partitioned = ListPartitioner.unequal_partition(this.list, 5);
		
		assertEquals(partitionedRef,partitioned);
	}
	
	@Test
	public void testPartition6_unequal(){
		List<List<Double>> partitionedRef = Arrays.asList(
				Arrays.asList(0.1,0.2,0.3,0.4,0.5,0.6));
		
		List<List<Double>> partitioned = ListPartitioner.unequal_partition(this.list, 6);
		
		assertEquals(partitionedRef,partitioned);
	}
	

}
