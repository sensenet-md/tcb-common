package util;

import static org.junit.Assert.*;

import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

import com.tcb.common.util.SafeMap;

public class SafeMapTest {

	private SafeMap<Integer, String> map;

	@Before
	public void setUp() throws Exception {
		this.map = new SafeMap<Integer,String>();
		map.put(1, "a");
	}

	@Test
	public void shouldGet() {
		assertEquals("a",map.get(1));
	}
	
	@Test
	public void shouldPutAll(){
		HashMap<Integer,String> map2 = new HashMap<Integer,String>();
		map2.put(2, "b");
		map.putAll(map2);
		
		assertEquals("a",map.get(1));
		assertEquals("b",map.get(2));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void putShouldNotOverride(){
		map.put(1, "b");
	}
	
	@Test
	public void replaceShouldOverride(){
		map.replace(1, "b");
		assertEquals("b", map.get(1));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void replaceShouldThrowWithEmptyKey(){
		map.replace(2, "b");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void putAllShouldNotOverride(){
		HashMap<Integer,String> map2 = new HashMap<Integer,String>();
		map2.put(1, "b");
		map.putAll(map2);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void shouldThrowForWrongKey(){
		map.get("1");
	}

}
