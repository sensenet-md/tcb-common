package util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.tcb.common.util.Rounder;

public class RounderTest {
	
	private Double x = 10.23456789d;
	private Double x3Decimals = 10.235d;
	private Double x1Decimal = 10.2d;
	private Double x0Decimals = 10.0d;
	
	
	
	@Test
	public void shouldRoundToThreeDecimalsAfterPoint() {
		Double testRounded = Rounder.round(x, 3);
		
		assertEquals(x3Decimals,testRounded);
				
	}
	
	@Test
	public void shouldRoundToOneDecimalAfterPoint() {
		Double testRounded = Rounder.round(x, 1);
		
		assertEquals(x1Decimal,testRounded);
				
	}
	
	@Test
	public void shouldRoundToZeroDecimalsAfterPoint() {
		Double testRounded = Rounder.round(x, 0);
		
		assertEquals(x0Decimals,testRounded);
				
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void shouldThrowExceptionWhenLessThanNullDecimal(){
		Double testRounded = Rounder.round(x, -1);
		
	}
	
	@Test
	public void shouldAcceptNull(){
		Double testRounded = Rounder.round(null, 1);
		
		assertEquals(null,testRounded);
	}

}
