package com.tcb.common.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import com.tcb.common.exception.NoMatchesException;
import com.tcb.common.exception.TooManyMatchesException;


public class ListFilter {
	//@SuppressWarnings("unchecked")
	public static <T> List<T> filterList(List<T> list, Predicate<T> predicate) {
	    ArrayList<T> filtered = new ArrayList<T>();
	    for (T element:list){
	    	if(predicate.test(element)){
	        	filtered.add(element);
	        }
	    }
	    filtered.trimToSize();
	    return filtered;
	}
	
	public static <T> List<T> removeFromList(List<T> list, Predicate<T> predicate) {
	    ArrayList<T> filtered = new ArrayList<T>();
	    for (T element:list){
	    	if(!predicate.test(element)){
	        	filtered.add(element);
	        }
	    }
	    filtered.trimToSize();
	    return filtered;
	}
	
	
	
	public static <T> Optional<T> singleton(Collection<T> collection) {
		if(collection.size() == 1){
			return Optional.of(collection.iterator().next());
		}
		else if(collection.size() == 0) {
			return Optional.empty();
		}
		else{
			throw new TooManyMatchesException("A list required to be of length 1 had a bigger size; Most likely a lookup required "
					+ "to give a unique result did not.");
		}
	}
	
}
