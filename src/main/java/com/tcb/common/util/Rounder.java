package com.tcb.common.util;

public class Rounder {
	public static Double round(Double d, Integer decimalsAfterPoint){
		if(d==null) return null;
		assertValidDecimals(decimalsAfterPoint);
		Double inversePrecision = Math.pow(10,decimalsAfterPoint);
		return (double) Math.round(d * inversePrecision) / inversePrecision;
	}
	
	private static void assertValidDecimals(Integer decimals){
		if(decimals < 0){
			throw new IllegalArgumentException("Cannot round to less than one decimal precision.");
		}
	}
	
	
}
