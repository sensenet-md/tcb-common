package com.tcb.common.util;

public class ArrayUtil {

	public static double getMax(double[] arr){
		double max = arr[0];
		for(double d:arr){
			if(d>max){
				max = d;
			}
		}
		return max;
	}
	
	public static double getMin(double[] arr){
		double min = arr[0];
		for(double d:arr){
			if(d<min){
				min = d;
			}
		}
		return min;
	}
	
	public static double getAverage(double[] arr){
		double sum = 0d;
		for(double d:arr){
			sum += d;
		}
		return sum / arr.length;
	}
	
	public static int indexOf(double[] arr, double value){
		for(int i=0;i<arr.length;i++){
			if(arr[i]==value){
				return i;
			}
		}
		return -1;
	}
}
