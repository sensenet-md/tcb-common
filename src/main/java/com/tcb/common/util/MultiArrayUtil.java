package com.tcb.common.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.tcb.common.exception.NonSymmetricArrayException;

public class MultiArrayUtil {
	public static String[][] transpose(String[][] arr) throws ArrayIndexOutOfBoundsException {
		int yLength = getSymmetricYindex(arr);
		String[][] transposed = new String[yLength][arr.length];
		for(int x=0;x<arr.length;x++){
			for(int y=0;y<yLength;y++){
				transposed[y][x] = arr[x][y];
			}
		}
		return transposed;
	}
	
	public static void checkSymmetric(String[][] arr){
		getSymmetricYindex(arr);
	}
	
	public static int getSymmetricYindex(String[][] arr){
		int y = arr[0].length;
		for(int i=1;i<arr.length;i++){
			if(arr[i].length != y) throw new NonSymmetricArrayException("Array is not symmetric");
		}
		return y;
	}
	
	public static List<List<String>> doubleArrayToLists(String[][] arr){
		List<List<String>> lists = new ArrayList<List<String>>();
		for(int x=0;x<arr.length;x++){
			lists.add(Arrays.asList(arr[x]));
		}
		return lists;
	}
	
}
