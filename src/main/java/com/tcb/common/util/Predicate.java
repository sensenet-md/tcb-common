package com.tcb.common.util;

public interface Predicate<T> {
    boolean test(T t);
}