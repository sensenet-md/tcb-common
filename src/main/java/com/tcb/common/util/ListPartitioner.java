package com.tcb.common.util;

import java.util.ArrayList;
import java.util.List;

public class ListPartitioner {
	// Divides a list into n sublists of chunk size m.
	// If the last sublist would have fewer than m elements, it is dropped.
	// Therefore, it is guaranteed that all sublists are of equal size, but not all elements might be utilized
	
	// End indices used here are exclusive
	public static <T> List<List<T>> equal_partition(List<T> list, int chunkSize) throws IllegalArgumentException {
		validateInput(list,chunkSize);
		int maxIndex = list.size();
		List<List<T>> result = new ArrayList<List<T>>();
		int startIndex = 0;
		int endIndex = chunkSize;
		
		while(endIndex <= maxIndex){
			List<T> subList = list.subList(startIndex, endIndex);
			result.add(subList);
			startIndex = startIndex + chunkSize;
			endIndex = endIndex + chunkSize;
		}
		return result;	
	}
	
	
	public static <T> List<List<T>> unequal_partition(List<T> list, int chunkSize) throws IllegalArgumentException {
		validateInput(list,chunkSize);
		int maxIndex = list.size();
		List<List<T>> result = new ArrayList<List<T>>();
		int startIndex = 0;
		int endIndex = chunkSize;
		
		while(startIndex < maxIndex){
			List<T> subList = list.subList(startIndex, endIndex);
			result.add(subList);
			startIndex = startIndex + chunkSize;
			endIndex = Math.min(endIndex + chunkSize,maxIndex);
		}
		return result;	
	}

 	
	private static <T> void validateInput(List<T> list, int chunkSize) throws IllegalArgumentException {
		if(list.size()==0) throw new IllegalArgumentException("List must contain at least one element");
		if(chunkSize < 1) throw new IllegalArgumentException("Chunk size must be at least 1");
		if(chunkSize > list.size()) throw new IllegalArgumentException("Chunk size must not be larger than list length.");
	}
	
}
