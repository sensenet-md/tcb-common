package com.tcb.common.util;

public class Tuple<A,B> {
	private A a;
	private B b;

	public Tuple(A a, B b){
		this.a = a;
		this.b = b;
	}
	
	public A one(){
		return a;
	}
	
	public B two(){
		return b;
	}
	
	@Override
	public String toString(){
		return String.format("(%s %s,%s %s)", a.getClass().toString(), a.toString(),
				b.getClass().toString(), b.toString());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((a == null) ? 0 : a.hashCode());
		result = prime * result + ((b == null) ? 0 : b.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tuple other = (Tuple) obj;
		if (a == null) {
			if (other.a != null)
				return false;
		} else if (!a.equals(other.a))
			return false;
		if (b == null) {
			if (other.b != null)
				return false;
		} else if (!b.equals(other.b))
			return false;
		return true;
	}

}
