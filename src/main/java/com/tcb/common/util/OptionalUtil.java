package com.tcb.common.util;

import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;

public class OptionalUtil {
	public static <T,U> Optional<U> applyIfPresent(Optional<T> innerOptional , Function<T,U> mapper){
			if(innerOptional.isPresent()) return Optional.of(mapper.apply(innerOptional.get()));
			else return Optional.empty();
		}
}

