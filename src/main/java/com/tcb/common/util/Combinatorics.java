package com.tcb.common.util;

import java.util.ArrayList;
import java.util.List;

public class Combinatorics {

	public static <T> List<Tuple<T,T>> getCombinationsNoSelf(List<T> lst){
		List<Tuple<T,T>> result = new ArrayList<Tuple<T,T>>();
		for(int i=0;i<lst.size();i++){
			for(int j=i+1;j<lst.size();j++){
				result.add(new Tuple<T,T>(lst.get(i),lst.get(j)));
			}
		}
		return result;
	}
	
	public static <T> List<Tuple<T,T>> getCartesianProduct(List<T> lst1, List<T> lst2){
		List<Tuple<T,T>> result = new ArrayList<Tuple<T,T>>();
		for(int i=0;i<lst1.size();i++){
			for(int j=0;j<lst2.size();j++){
				result.add(new Tuple<T,T>(lst1.get(i),lst2.get(j)));
			}
		}
		return result;
	}
}
