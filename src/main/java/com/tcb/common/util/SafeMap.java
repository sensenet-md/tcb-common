package com.tcb.common.util;

import java.util.HashMap;
import java.util.Map;

public class SafeMap<K,V> extends HashMap<K,V> {

	private static final long serialVersionUID = 1L;

	@Override
	public V get(Object key){
		V value = super.get(key);
		if(value==null) 
			throw new IllegalArgumentException(String.format("No object stored for: %s", key.toString()));
		return super.get(key);
	}
	
	@Override
	public V put(K key, V value){
		if(super.containsKey(key))
			throw new IllegalArgumentException(String.format("Attempted override of key: %s", key.toString()));
		return super.put(key, value);			
	}
	
	public V putOrReplace(K key, V value){
		return super.put(key, value);
	}
	
	@Override
	public void putAll(Map<? extends K,? extends V> o){
		o.entrySet().stream()
		.forEach(e -> put(e.getKey(), e.getValue()));
	}
	
	public void putOrReplaceAll(Map<? extends K,? extends V> o){
		super.putAll(o);
	}
	
	@Override
	public V replace(K key, V value){
		get(key);
		return super.put(key, value);
	}
	
}
