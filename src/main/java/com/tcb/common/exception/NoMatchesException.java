package com.tcb.common.exception;

public class NoMatchesException extends RuntimeException {
	public NoMatchesException(String message){
		super(message);
	}
}
