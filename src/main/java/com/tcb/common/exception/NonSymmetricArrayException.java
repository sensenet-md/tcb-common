package com.tcb.common.exception;

public class NonSymmetricArrayException extends IllegalArgumentException {

	public NonSymmetricArrayException(String string) {
		super(string);
	}

}
