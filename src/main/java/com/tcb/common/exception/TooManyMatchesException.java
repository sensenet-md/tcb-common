package com.tcb.common.exception;

public class TooManyMatchesException extends RuntimeException {
	public TooManyMatchesException(String message){
		super(message);
	}
}
